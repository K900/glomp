use std::collections::HashMap;
use std::fs::File;
use std::io::{stdin, Read};

use brace_expand::brace_expand;
use clap::Parser as ClapParser;
use colored::*;
use nom::bytes::complete::{is_not, tag};
use nom::character::complete::{hex_digit1, line_ending, not_line_ending, space1};
use nom::combinator::opt;
use nom::multi::{many0, separated_list0};
use nom::sequence::{terminated, tuple};
use nom::{IResult, Parser};
use reqwest::Url;

fn parse_commit_line(stream: &str) -> IResult<&str, &str> {
    tuple((tag("commit "), hex_digit1, line_ending))
        .map(|(_, hash, _)| hash)
        .parse(stream)
}

fn parse_header_like(stream: &str) -> IResult<&str, (&str, &str)> {
    tuple((is_not(":\n"), tag(":"), space1, not_line_ending))
        .map(|(title, _, _, value)| (title, value))
        .parse(stream)
}

fn parse_headers(stream: &str) -> IResult<&str, HashMap<&str, &str>> {
    many0(terminated(parse_header_like, line_ending))
        .map(|pairs| pairs.into_iter().collect())
        .parse(stream)
}

fn parse_message_line(stream: &str) -> IResult<&str, &str> {
    tuple((tag("    "), opt(not_line_ending), line_ending))
        .map(|(_, words, _)| words.unwrap_or_default())
        .parse(stream)
}

#[derive(Debug)]
struct Message<'a> {
    body: Vec<&'a str>,
    trailers: Vec<(&'a str, &'a str)>,
}

fn parse_message_body(stream: &str) -> IResult<&str, Message> {
    many0(parse_message_line)
        .map(|msg| {
            let mut body = vec![];
            let mut trailers = vec![];

            let mut in_trailers = true;

            for item in msg.into_iter().rev() {
                if in_trailers {
                    if item.is_empty() {
                        continue;
                    }

                    if let Ok((left, parsed)) = parse_header_like(item) {
                        if left.is_empty() {
                            trailers.push(parsed);
                            continue;
                        }
                    }

                    body.push(item);
                    in_trailers = false;
                } else {
                    body.push(item);
                }
            }

            if matches!(body.last(), Some(&"")) {
                // remove empty line after title, if it exists
                body.pop();
            }

            body.reverse();

            Message { body, trailers }
        })
        .parse(stream)
}

#[allow(dead_code)]
#[derive(Debug)]
struct Commit<'a> {
    hash: &'a str,
    headers: HashMap<&'a str, &'a str>,
    title: &'a str,
    message: Vec<&'a str>,
    trailers: Vec<(&'a str, &'a str)>,
}

impl<'a> Commit<'a> {
    fn get_groups(&self) -> Vec<Vec<String>> {
        fn special(s: &str) -> Vec<Vec<String>> {
            return vec![vec![format!("<{}>", s)]];
        }

        let mut title = self.title;

        if title.starts_with("Merge") {
            return special("merges");
        }

        while let Some(stripped) = title.strip_prefix("Revert \"") {
            title = stripped
        }

        let subsystems = match title.rsplit_once(':') {
            Some((subsystems, _)) => subsystems,
            None => return special("unknown"),
        };

        if subsystems.contains('{') && subsystems.contains('}') {
            // brace_expand eats commas otherwise
            brace_expand(subsystems)
        } else {
            vec![subsystems.to_string()]
        }
        .iter()
        .flat_map(|x| x.split(','))
        .map(|x| {
            x.split(|ch| ch == ':' || ch == '/' || ch == ' ')
                .map(|x| x.trim())
                .filter(|x| !x.is_empty())
                .map(|x| x.to_lowercase())
                .collect()
        })
        .collect()
    }
}

fn parse_commit(stream: &str) -> IResult<&str, Commit> {
    tuple((
        parse_commit_line,
        parse_headers,
        line_ending,
        parse_message_line,
        parse_message_body,
    ))
    .map(|(hash, headers, _, title, message)| Commit {
        hash,
        headers,
        title,
        message: message.body,
        trailers: message.trailers,
    })
    .parse(stream)
}

fn parse_commits(stream: &str) -> IResult<&str, Vec<Commit>> {
    separated_list0(line_ending, parse_commit)(stream)
}

#[derive(ClapParser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    source: String,
}

fn main() {
    let args = Args::parse();

    let mut buf = Vec::new();

    match args.source.as_ref() {
        "-" => {
            stdin()
                .lock()
                .read_to_end(&mut buf)
                .expect("Failed to read");
        }
        x => {
            if let Ok(u) = Url::parse(x) {
                reqwest::blocking::get(u)
                    .expect("Failed to get")
                    .read_to_end(&mut buf)
                    .expect("Failed to read");
            } else {
                File::open(x)
                    .expect("Failed to open file")
                    .read_to_end(&mut buf)
                    .expect("Failed to read");
            }
        }
    }
    let log = String::from_utf8(buf).expect("Failed to decode");
    let commits = parse_commits(&log).expect("Failed to parse").1;

    let mut grouped_commits: HashMap<Vec<String>, Vec<&Commit>> = HashMap::new();

    for commit in &commits {
        for group in commit.get_groups() {
            grouped_commits.entry(group).or_default().push(commit);
        }
    }

    let mut keys: Vec<_> = grouped_commits.keys().collect();
    keys.sort();

    for key in keys.into_iter() {
        println!("{}", key.join("/").blue());

        for commit in &grouped_commits[key] {
            println!("  - {}", commit.title.green());
        }
    }
}
