{
  description = "git log message parser, definitely not backronymed";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable-small";
    flake-utils.url = "github:numtide/flake-utils";
    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
      pre-commit-hooks,
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        packages = rec {
          glomp = pkgs.rustPlatform.buildRustPackage {
            name = "glomp";
            version = "1.0.0";

            src = ./.;
            cargoLock = {
              lockFile = ./Cargo.lock;
            };

            # skips rebuilding the whole thing with debug info
            doCheck = false;
          };
          default = glomp;
        };

        devShells.default = pkgs.mkShell {
          RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";

          inputsFrom = builtins.attrValues self.packages.${system};
          nativeBuildInputs = [
            pkgs.cargo-outdated
            pkgs.rustfmt
            pkgs.clippy
            pkgs.pkg-config
          ];
          buildInputs = [ pkgs.openssl ];
          inherit (self.checks.${system}.pre-commit-check) shellHook;
        };

        checks.pre-commit-check = pre-commit-hooks.lib.${system}.run {
          src = ./.;
          hooks = {
            nixfmt-rfc-style.enable = true;
            deadnix.enable = true;
            statix.enable = true;

            rustfmt.enable = true;
            clippy.enable = true;
            cargo-check.enable = true;
          };
        };
      }
    );
}
